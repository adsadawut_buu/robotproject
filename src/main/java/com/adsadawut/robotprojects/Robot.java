/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.adsadawut.robotprojects;

/**
 *
 * @author อัษฎาวุฒิ
 */
public class Robot {

    private int x;
    private int y;
    private char Symbol;
    private TableMap map;

    public Robot(int x, int y, char Symbol, TableMap map) {
        this.x = x;
        this.y = y;
        this.Symbol = Symbol;
        this.map = map;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public char getSymbol() {
        return Symbol;
    }

    public boolean walk(char direction) {
        switch(direction){
            case 'N':
            case 'w':
                if(wallkN())return false;
                break;
            case 'S':
            case 's':
                if(walkS())return false;
                break;
            case 'E':
            case 'd':
                if(walkE())return false;
                break;
            case 'W':
            case 'a':
                if(walkW())return false;
                 break;
                
        }
        checkBomb();
        return true;
    }

    private void checkBomb() {
        if(map.isBomb(x,y)){
            System.out.println("Found Bomb!!!" + "("+x+", "+y+")");
            System.out.println("Finish Project!!!");
        }
    }

    private boolean walkW() {
        if (map.inMap(x-1,y)) {
            x=x-1;
        } else {
            return true;
        }
        return false;
    }

    private boolean walkE() {
        if (map.inMap(x+1, y)) {
            x=x+1;
        } else {
            return true;
        }
        return false;
    }

    private boolean walkS() {
        if (map.inMap(x,y+1)) {
            y=y+1;
        } else {
            return true;
        }
        return false;
    }

    private boolean wallkN() {
        if (map.inMap(x, y-1)) {
            y=y-1;
        } else {
            return true;
        }
        return false;
    }

    public boolean isOn(int x, int y) {
        return this.x == x && this.y == y;
    }
}
